// url (required), options (optional)
fetch('https://jsonblob.com/api/9bc50e8e-f43b-11e7-b1e2-3d54d64c6b3c', {
	method: 'get'
})
	.then(function(response) {
		return response.json();
	})
	.then(res => {
		console.log(res);
		document.getElementById('fetchData').innerHTML = res.user;
	})
	.catch(err => {
		console.error('err: ', err);
	});

// Es5
var calculateMonthlyPayment = function (principal, years, rate) {
    if (rate) {
        var monthlyRate = rate / 100 / 12;
    }
    var monthlyPayment = principal * monthlyRate / (1 - (Math.pow(1 / (1 + monthlyRate), years * 12)));
    return monthlyPayment;
};

document.getElementById('calcBtn').addEventListener('click', function () {
    var principal = document.getElementById("principal").value;
    var years = document.getElementById("years").value;
    var rate = document.getElementById("rate").value;
    var monthlyPayment = calculateMonthlyPayment(principal, years, rate);
    document.getElementById("monthlyPayment").innerHTML = monthlyPayment.toFixed(2);
});


// Es6 with let block Scope

// let calculateMonthlyPayment = function (principal, years, rate) {
//     if (rate) {
//         let monthlyRate = rate / 100 / 12;
//     }
//     let monthlyPayment = principal * monthlyRate / (1 - (Math.pow(1 / (1 + monthlyRate), years * 12)));
//     return monthlyPayment;
// };
//
// document.getElementById('calcBtn').addEventListener('click', function () {
//     let principal = document.getElementById("principal").value;
//     let years = document.getElementById("years").value;
//     let rate = document.getElementById("rate").value;
//     let monthlyPayment = calculateMonthlyPayment(principal, years, rate);
//     document.getElementById("monthlyPayment").innerHTML = monthlyPayment.toFixed(2);
// });

// //Es6 with const block Scope
//
// const rate = 5.0;
//
// //  rate = 10. 0;
//
// const obj = {
//   msg : "HEllO WORLD !!!!"
// };
//
// //  obj.msg = "BYE BYE !!!";
//
// //  obj = {};
//
// document.getElementById('constRate').innerHTML = rate;
//
// document.getElementById('constObj').innerHTML = obj.msg;

console.log('Loading...');

function someAsyncFn(param1, param2) {
	const p = new Promise(function(resolve, reject) {
		setTimeout(() => {
			const res = param1 + param2;
			console.log('res: ', res);
			if (res > 100) {
				resolve(res);
			} else {
				reject('Less than eddot');
			}
		}, 0);
	});

	return p;
}

const output = someAsyncFn(200, 20);
console.log('output: ', output);

output
	.then(res => {
		console.log('res : ', res);
		return res + 100;
	})
	.then(res => {
		console.log('res : ', res);
	})
	.catch(err => {
		console.log('err: ', err);
	});

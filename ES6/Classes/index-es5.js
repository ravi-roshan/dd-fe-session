function Person(name, age) {
	this.name = name;
	this.age = age;
}
Person.prototype.introducePerson = function name() {
	console.log(`${this.name} is ${this.age} years old`);
};

function Employee(name, age, company, dept) {
	Person.call(this, name, age);
	this.company = company;
	this.dept = dept;
}
Employee.prototype = Person.prototype;
Employee.prototype.work = function() {
	console.log(`${this.name} is ${this.age} years old and working in ${this.company}`);
};

const p1 = new Person('Ravi', 26);
console.log('p1: ', p1);

p1.introducePerson();

const e1 = new Employee('Ravi', 26, 'Deloitte', 'JavaScript');
console.log('e1: ', e1);

e1.introducePerson();
e1.work();

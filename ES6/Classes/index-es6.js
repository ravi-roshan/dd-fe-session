class Person {
	constructor(name, age) {
		this.name = name;
		this.age = age;
	}

	introducePerson() {
		console.log(`${this.name} is ${this.age} years old`);
	}
}

class Employee extends Person {
	constructor(name, age, company, dept) {
		super(name, age);
		this.company = company;
		this.dept = dept;
	}

	work() {
		console.log(`${this.name} is ${this.age} years old and working in ${this.company}`);
	}
}

const p1 = new Person('Ravi', 26);
console.log('p1: ', p1);

p1.introducePerson();

const e1 = new Employee('Roshan', 26, 'Deloitte', 'JavaScript');
console.log('e1: ', e1);

e1.introducePerson();
e1.work();

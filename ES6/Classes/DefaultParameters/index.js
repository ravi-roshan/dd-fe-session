// Es5 code . :
var es5Defualt = function (height, color) {
    var height = height || 50
    var color = color || 'red'

    console.log('Es5 ---- ', height);
}

es5Defualt();

// Es6 Code :

var es6Default = function (height = 500, color = 'black') {
    console.log('Es6 ---- ', height);
}


es6Default(undefined);


//useCases :  undefined , null


// Default parameters are available to later default parameters

function singularAutoPlural(singular, plural = `${singular}\'s`,
                            game = plural + ' FIGHT!!!') {
  console.log(singular, plural, game);
}

//["Pen","Pen's", "Pen's  FIGHT"]
singularAutoPlural('Pen');


// Destructured parameter with default value assignment
function add([x, y] = [1, 2], z = 3) {
  return x + y + z;
}

console.log(add()); // 6

function sum(num1 = num2, num2= 10){
  return num1 + num2;
}

console.log('sum ----- ',sum(undefined,50));

// limited params

// function add(num1, num2){
//   return num1 + num2;
// }

// const result  = add(5, 10);

// // const result  = add(5, 10 , 10);
// console.log(result);


// with Arguments

// function add(){
//   let result = 0;
//   for(i=0; i< arguments.length; i++) {
//     result+= arguments[i];
//   }
//   return result;
// }

// const result  = add(5, 10);

// // const result  = add(5, 10 , 10);
// console.log(result);

// console.log(add(5,10,15,20));

// Spread Operator


function add(...nums){
    let result = 0;
  for(i=0; i< arguments.length; i++) {
    result+= arguments[i];
  }
  return result;
}

const result  = add(5, 10);

// const result  = add(5, 10 , 10);
console.log(result);
console.log(add(5,10,15,20));


// taking rest params into an array

 const  printEmployeeSalary = (name, company, ...monthlySalaries) => {

   console.log(name);
   console.log(company);
   console.log(monthlySalaries);
 }

 printEmployeeSalary('John', 'Deloitte' , 10, 20, 30);


// Spread in Array Literals

var dateFields = [2018, 0, 15];  // 1 Jan 1970
var d = new Date(...dateFields);

console.log(d);

// Es5
function getES5Template(jsVersion) {
  return 'We are using ' + jsVersion + ' syntax .';
}

document.getElementById('es5Template').innerHTML = getES5Template('ES5');

//Es6

function getES6Template(jsVersion) {
  return `We are using  ${jsVersion} syntax`;
}

document.getElementById('es6Template').innerHTML = getES6Template('ES6');

var index = 0 ;

var resultString  = `The result is ${ index === 0 ? true : false}`;

document.getElementById('es6expressions').innerHTML = resultString;


console.log('string text line 1\n' +
'string text line 2');

console.log(`string text line 1
string text line 2`);




// Es5 :
var response = {
  userName :  'Sid',
  firstName : 'Siddhant',
  lastName : 'Wadhera'
};


var firstName = response.firstName;
var lastName = response.lastName;

document.getElementById('es5DataUserName').innerHTML = firstName + ' ' + lastName;

// Es6

var response = {
  userName :  'Sid',
  firstName : 'Siddhant',
  lastName : 'Wadhera Es6'
};


var {lastName, firstName} = response;

document.getElementById('es6DataUserName').innerHTML = `${firstName}  ${lastName}`;

const obj1 = {
  name: 'Ravi',
  company: 'Deloitte'
}

const obj2 = {
  name: 'Sid',
  company: 'Deloitte'
}


function foo() {
  console.log('this : ',this);
}

const bar = () => {
  console.log('this : ',this);
}


function newFoo() {
  console.log('this outer : ',this);

  const innerFoo = function() {
     console.log('this inner : ',this);
  }

  // const innerFoo = () => {
  //    console.log('this inner : ',this);
  // }

  return innerFoo;
}



// foo.bind(obj2)();

// bar.bind(obj2)();

const innerFn = newFoo.bind(obj1)();

innerFn();




var materials = [
  'Hydrogen',
  'Helium',
  'Lithium',
  'Beryllium'
];

const arrLength = materials.map((material) => {
  return material.length;
}); // [8, 6, 7, 9]

console.log(arrLength);


// no binding of arguments

function bindArgs() {
    console.log(arguments);
    console.log(arguments[0]);
}

bindArgs(1,2);


const bindArgsArrow = () => {
    console.log(arguments);
    console.log(arguments[0]);
}

bindArgsArrow(1,2);


// Cannot use as Constructor Function

function Person(age, name) {
  this.age = age;
  this.name = name;
}

const p1 = new Person(23, 'ST');

console.log('p1-constructor ::', p1);

const Employee = (company, name) =>  {
  this.company = company;
  this.name = name;
}

const e1 = new Employee('Deloitte', 'ST');

console.log('e1-constructor ::', e1);

// Does not work , as it does not have this scoped.
